package routeplanning;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class RoutePlanning {
	List<Route> route = new ArrayList<>();

	void storeRoutesInList() {
		String line;
		try {
			BufferedReader br = new BufferedReader(new FileReader("routes.txt"));
			while ((line = br.readLine()) != null) {
				String arr[] = line.split(",");
				route.add(new Route(arr[0], arr[1], arr[2], arr[3], arr[4]));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	void displayRotesFromList() {
		System.out.format("%10s %30s %20s %20s %20s", "From", "To", "Distance", "Travel time", "Amount\n");
		for (Route i : route) {
			System.out.format("%10s %30s %20s %20s %20s", i.getRouteFrom(), i.getRouteTo(), i.getDistance(),
					i.getTime(), i.getAmount());
			System.out.println();
		}
	}

	void showDirectFlights(ArrayList<Route> routeInfo, String fromCity) {
		System.out.format("%10s %30s %20s %20s %20s", "From", "To", "Distance", "Travel time", "Amount\n");
		for (Route i : route) {
			for (Route j : routeInfo) {
				if (i.getRouteFrom().equals(fromCity) && j.getRouteTo().equals(i.getRouteTo())) {
					System.out.format("%10s %30s %20s %20s %20s", i.getRouteFrom(), i.getRouteTo(), i.getDistance(),
							i.getTime(), i.getAmount());
					System.out.println();
				}
			}
		}
	}

	void sortDirectFlighs(List<Route> routeInfo, String fromCity) {
		List<Route> sortedList = new ArrayList<>();
		for (Route i : route) {
			for (Route j : routeInfo) {
				if (i.getRouteFrom().equals(fromCity) && j.getRouteTo().equals(i.getRouteTo())) {
					sortedList.add(i);
				}
			}
		}
		Collections.sort(sortedList, new Route());
		System.out.format("%10s %30s %20s %20s %20s", "From", "To", "Distance", "Travel time", "Amount\n");
		for (Route i : sortedList) {
			System.out.format("%10s %30s %20s %20s %20s", i.getRouteFrom(), i.getRouteTo(), i.getDistance(),
					i.getTime(), i.getAmount());
			System.out.println();
		}
	}

	void displayRoutesWithIntermediateStations(String fromCity, String toCity) {
		System.out.format("%10s %30s %20s %20s %20s", "From", "To", "Distance", "Travel time", "Amount\n");
		for (int i = 0; i < route.size(); i++) {
			if (route.get(i).getRouteFrom().equals(fromCity)) {
				if (route.get(i).getRouteTo().contentEquals(toCity)) {
					System.out.format("%10s %30s %20s %20s %20s", route.get(i).getRouteFrom(),
							route.get(i).getRouteTo(), route.get(i).getDistance(), route.get(i).getTime(),
							route.get(i).getAmount());
					System.out.println();
				} else {
					for (int j = i + 1; j < route.size(); j++) {
						if (route.get(i).getRouteTo().equals(route.get(j).getRouteFrom())
								&& route.get(j).getRouteTo().equals(toCity)) {
							System.out.format("%10s %30s %20s %20s %20s", route.get(i).getRouteFrom(),
									route.get(i).getRouteTo(), route.get(i).getDistance(), route.get(i).getTime(),
									route.get(i).getAmount());
							System.out.println();
							System.out.format("%10s %30s %20s %20s %20s", route.get(j).getRouteFrom(),
									route.get(j).getRouteTo(), route.get(j).getDistance(), route.get(j).getTime(),
									route.get(j).getAmount());
							System.out.println();
						}
					}
				}
			}
		}
	}

	public static void main(String[] args) {
		ArrayList<Route> parameter = new ArrayList<>();
		parameter.add(new Route("Delhi", "Mumbai", "1148", "2:10", "INR 6000"));
		parameter.add(new Route("Delhi", "London", "1148", "2:10", "INR 6000"));
		RoutePlanning rp = new RoutePlanning();
		rp.storeRoutesInList();
		// rp.displayRotesFromList();
		rp.showDirectFlights(parameter, "Delhi");
		rp.sortDirectFlighs(parameter, "Delhi");
		rp.displayRoutesWithIntermediateStations("Delhi", "London");
	}
}
