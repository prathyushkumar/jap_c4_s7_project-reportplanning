package routeplanning;

import java.util.Comparator;

import routeplanning.Route;

public class Route implements Comparator<Route>{
	private String routeFrom;
	private String routeTo;
	private String distance;
	private String time;
	private String amount;
	public Route() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Route(String routeFrom, String routeTo, String distance, String time, String amount) {
		super();
		this.routeFrom = routeFrom;
		this.routeTo = routeTo;
		this.distance = distance;
		this.time = time;
		this.amount = amount;
	}
	public String getRouteFrom() {
		return routeFrom;
	}
	public void setRouteFrom(String routeFrom) {
		this.routeFrom = routeFrom;
	}
	public String getRouteTo() {
		return routeTo;
	}
	public void setRouteTo(String routeTo) {
		this.routeTo = routeTo;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "Route [routeFrom=" + routeFrom + ", routeTo=" + routeTo + ", distance=" + distance + ", time=" + time
				+ ", amount=" + amount + "]";
	}
	
	public int compare(Route o1, Route o2) {
		// TODO Auto-generated method stub
		return o1.routeTo.compareTo(o2.routeTo);
	}
	
	
}